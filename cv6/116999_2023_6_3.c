#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 256

int samohlasky(char * ch,int s,int sum){
    if(ch[s] == ' ' || ch[s] == '\n'){
        return sum;
    }
    if(ch[s] == 'a' || ch[s] == 'A'){sum++;}
    if(ch[s] == 'e' || ch[s] == 'E'){sum++;}
    if(ch[s] == 'i' || ch[s] == 'I'){sum++;}
    if(ch[s] == 'o' || ch[s] == 'O'){sum++;}
    if(ch[s] == 'u' || ch[s] == 'U'){sum++;}
    if(ch[s] == 'y' || ch[s] == 'Y'){sum++;}
    return samohlasky(ch, ++s,sum);
}


int main(){
    char * line = malloc(sizeof(char) * SIZE);
    fgets(line, SIZE, stdin);
    int start = 0;
    for (int i = 0; i<strlen(line); i++) {
        if (line[i] == ' ' || line[i] == '\n') {
            int size = samohlasky(line + start, 0,0);
            printf("%d ", size);
            start = i + 1;
        }
    }
    printf("\n");
    free(line);
    line = NULL;
    return 0;
}
