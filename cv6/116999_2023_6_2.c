#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 256

int dlzka(char * ch,int s){
    if(ch[s] == ' ' || ch[s] == '\n'){
        return s;
    }
    return dlzka(ch, ++s);
}


int main(){
    char * line = malloc(sizeof(char) * SIZE);
    fgets(line, SIZE, stdin);
    int w_size_max = 0;
    int start = 0;
    for (int i = 0; i<strlen(line); i++) {
        if (line[i] == ' ' || line[i] == '\n') {
            int size = dlzka(line + start, 0);
            printf("%d ", size);
            if (w_size_max < size) {
                w_size_max = size;
            }
            start = i + 1;
        }
    }
    printf("\nNajdlhsie slovo ma %d znakov.\n",w_size_max);

    free(line);
    line = NULL;
    return 0;
}
