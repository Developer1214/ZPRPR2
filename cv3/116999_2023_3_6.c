#include<stdio.h>
#include<stdlib.h>
#include <string.h>

void create_hashmap(int len,int* hashmap){
    memset(hashmap,0,len*sizeof(int));
}

void print_riadok(int* hashmap,int len){
    for (int i = 0; i < len; i++) {
        if(hashmap[i-1] >= 99) printf("%d",hashmap[i]);
        else printf(" %d",hashmap[i]);
    }
    printf("\n");
    create_hashmap(len, hashmap); 
}

int main(){
    FILE *f;
    char * file;
    scanf("%s",file);
    f = fopen(file,"r");
    int len_hashmap = 26;
    int * hashmap = malloc(len_hashmap*sizeof(int));
    create_hashmap(len_hashmap, hashmap);
    if(f == NULL){
        printf("Nepodarilosa otvorit subor %s\n",file);
        return 1;
    }
    printf(" A B C D E F G H I J K L M N O P Q R S T U V W X Y Z\n");
    for(int i = 0;feof(f) == 0;i++){
        char c = getc(f);

        if (c == ' ' || c == '\n') {
            continue;
        }
        if (c == '.') {
            print_riadok(hashmap,len_hashmap);
            continue;
        }
        if(c >= 'A' && c <= 'Z'){
            //Debug code here
            //printf("Cislo %d(%c) - 'A'(%d) res - %d\n",c,c,'A',c - 'A');
            if(hashmap[c - 'A'] == 99){
                continue;
            }
            hashmap[c - 'A']++;
        }else if(c >= 'a' && c <= 'z'){
            //Debug here
            //printf("Cislo %d(%c) - 'a'(%d) res - %d\n",c,c,'a',c - 'a');
            if(hashmap[c - 'a'] == 99){
                continue;
            }
            hashmap[c - 'a']++;
        }
    }
    
    free(hashmap);
    return 0;
}
