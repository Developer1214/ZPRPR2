#include <stdio.h>
#include <stdlib.h>


void allokuj(char*** p,int n){
    *p = (char**)malloc(n * sizeof(char*));
    for(int i = 0;i<n;i++){
        (*p)[i] = (char*)malloc(sizeof(char) * 26);
    }
}

void nacitaj(char **p,int n){
    printf("Zadajte retazce\n");
    for(int i = 0;i< n;i++){
        scanf("%25s",p[i]);
        int c;
        while ((c = getchar()) != '\n' && c != EOF) { }
        fflush(stdin);
    }
}

void vypis(char **p,int n){
    printf("Zadali ste\n");
    for(int i = 0; i<n; i++) {
        printf("%s\n",p[i]);  
    }

}

int main(){
    int n = 0;
    char ** p;
    printf("Zadajte prve cislo\n");
    scanf("%d",&n);
    allokuj(&p,n);
    nacitaj(p, n); 
    vypis(p,n);
    for(int i = 0;i<n;i++){
        free(p[i]);
    }

    free(p);
    return 0;
}
