#include <stdio.h>
#include <stdlib.h>


void allokuj(int** p,int n){
    *p = (int *)malloc(n * sizeof(int));
}

void nacitaj(int *p,int n){
    printf("Zadajte cisla\n");
    for(int i = 0;i< n;i++){
        scanf("%d",&p[i]);
    }
}

void vypis(int * p,int n){
    printf("Zadali ste:\n");
    for(int i = 0; i<n; i++) {
        printf("%d\n",p[i]);  
    }

}

int main(){
    int n;
    int * p;
    printf("Zadajte prve cislo\n");
    scanf("%d",&n);
    allokuj(&p,n);
    nacitaj(p, n); 
    vypis(p,n);
    free(p);
    return 0;
}
