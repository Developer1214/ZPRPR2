#include <stdio.h>
#include <stdlib.h>


int main(){
    int n;
    printf("Zadajte prve cislo\n");
    scanf("%d",&n);
    int *p = malloc(n * sizeof(int));
    printf("Zadajte cisla\n");
    for(int i = 0;i< n;i++){
        scanf("%d",&p[i]);
    }

    printf("Zadali ste\n");
    for(int i = 0; i<n; i++) {
        printf("%d\n",p[i]);  
    }
    free(p);
    return 0;
}
