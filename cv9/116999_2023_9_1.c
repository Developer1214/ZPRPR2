#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef enum {
    FOR_UNIT,
    FOR_KILO,
} PriceType;

typedef union {
    float for_unit;
    float for_kilo;
} PriceValue;

typedef struct item{
    struct item * next;
    char * name;
    char * manuf;
    int count;
    PriceType type;
    PriceValue price;
} Item;

void clear_stdin(){
    char c;
    while ((c = getchar()) != '\n' && c !=EOF) {
    }
}

void add(Item ** startItem){
    Item * newItem = (Item *)malloc(sizeof(Item));
    newItem->name = (char *)malloc(sizeof(char) * 51); 
    newItem->manuf = (char *)malloc(sizeof(char) * 51);
    newItem->count = 0;
    newItem->next = NULL;
    fgets(newItem->name,51,stdin);
    newItem->name[strcspn(newItem->name, "\n")] = '\0';
    fgets(newItem->manuf,51,stdin);
    newItem->manuf[strcspn(newItem->manuf, "\n")] = '\0';

    char * buf = malloc(sizeof(char)*50);
    fgets(buf, 50, stdin);
    sscanf(buf, "%d",&newItem->count);
    printf("Choose type 1 for unit 2 for kilo and then type price: [type] [price]\n");
    fgets(buf, 50, stdin);
    int priceType;
    float price;
    sscanf(buf, "%d %f", &priceType, &price);
    
    if (priceType == 1) {
        newItem->type = FOR_UNIT;
        newItem->price.for_unit = price;
    } else if (priceType == 2) {
        newItem->type = FOR_KILO;
        newItem->price.for_kilo = price;
    } else {
        printf("Invalid price type. Please choose 1 for unit or 2 for kilo.\n");
        free(buf);
        buf = NULL;
        return;
    }
    free(buf);
    buf = NULL;
    if(*startItem == NULL || strcmp((*startItem)->name, newItem->name) > 0){
        newItem->next = *startItem;
        *startItem = newItem;
    } else {
        Item * previous = *startItem;
        Item * present = (*startItem)->next;
        while(present != NULL && strcmp(present->name, newItem->name) < 0){
            previous = present;
            present = present->next;
        }
        newItem->next = present;
        previous->next = newItem;
    }
}

void delete(Item **startItem) {
    if (*startItem == NULL) {
        printf("Not entered yet\n");
        return;
    }
    
    char * name = (char *)malloc(sizeof(char) * 51);
    fgets(name, 51, stdin);
    name[strcspn(name, "\n")] = '\0';
    printf("Name-%s",name); 

    Item * presentItem = *startItem;
    Item * previousItem = NULL;
    while (presentItem != NULL) {
        if (strcmp(presentItem->name, name) == 0) {
            if (previousItem == NULL) {
                *startItem = presentItem->next;
            } else {
                previousItem->next = presentItem->next;
            }

            free(presentItem->name);
            free(presentItem->manuf);
            free(presentItem);
            break;
        } else {
            previousItem = presentItem;
            presentItem = presentItem->next;
        }
    }
    free(name);
    name = NULL;
}

void close(Item ** startItem){
    Item * presentItem = *startItem;
    while(presentItem != NULL) {
        Item * nextItem = presentItem->next;
        free(presentItem->name);
        presentItem->name = NULL;
        free(presentItem->manuf);
        presentItem->manuf = NULL;
        presentItem->count = 0;
        presentItem = nextItem;
    }
    presentItem = NULL;
}

void print(Item **startItem){
    if(*startItem == NULL){
        printf("Not entred yet\n");
        return;
    }
    Item * presentItem = *startItem;
    while(presentItem != NULL){
        printf("Name-%s\n",presentItem->name);
        printf("Manufacture-%s\n",presentItem->manuf);
        printf("Count-%d\n",presentItem->count);
        if (presentItem->type == FOR_UNIT) {
            printf("Price per unit: %.2f\n", presentItem->price.for_unit);
        } else if (presentItem->type == FOR_KILO) {
            printf("Price per kilogram: %.2f\n", presentItem->price.for_kilo);
        }
        presentItem = presentItem->next;
    }
}

int main(){
    char comma;
    Item * startItem = NULL;
    while (1) {
        scanf("%1c",&comma);
        clear_stdin();
        switch (comma) {
            case 'a': add(&startItem);break;
            case 'd': delete(&startItem);break;
            case 'k': close(&startItem);break;
            case 'p': print(&startItem);break;
            default: printf("Neznamy prikaz\n");break;
        }
    }
    return 0;
}
