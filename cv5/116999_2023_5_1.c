#include <stdio.h>
#include <stdlib.h>
int main(){
    int n;
    scanf("%d",&n);
    char abceda = 'a';
    
    for(int i = 0;i<n;i++){
        for(int j = 0;j<n;j++){
            char ch;

            if (i % 2 != 0) {
                ch = abceda + n - j - 1;
            } else {
                ch = abceda + j;
            }
            if (ch > 'z' || ch < 'a') {
                ch = 'a' + (ch - 'z' - 1);
            }
            printf("%c ",ch);
        }
        abceda += n;
        printf("\n");
    }
    return 0;
}
