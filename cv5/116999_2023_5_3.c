#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(){
    int n;
    scanf("%d",&n);

    char ** str = malloc(sizeof(char*)*n);
    for (int i = 0; i<n; i++) {
        str[i] = (char*)malloc(sizeof(char) * 1024);
        scanf("%1024s",str[i]);
        int c;
        while ((c = getchar()) != '\n' && c != EOF){}
        fflush(stdin);
    }
    for (int i = 0; i<n; i++) {
        printf("V %d vete %ld znakov\n",i+1,strlen(str[i]));
    }
    for (int i = 0; i<n; i++) {
        if(strstr(str[i], "ako") != NULL){
            printf("V slove %s je slovo ako\n", str[i]);
        }
        for (int j = 0; j<strlen(str[i]); j++) {
            if(str[i][j] == 'A'){
                printf("Tu je pismeno A %s\n",str[i]);
                break;
            }
        }
    }


    for (int i = 0; i<n; i++) {
        free(str[i]);
        str[i] = NULL;
    }
    free(str);
    str = NULL;
    
    return 0;
}
