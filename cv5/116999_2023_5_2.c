#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define N 20

bool magic(int ***p,int n){
    if(*p == NULL)
        return false;
    int sumVert = 0;
    int sumHorizont = 0;
    int sumDiagonal = 0;
    //vertical
    for (int i = 0; i<n; i++) {
        for (int j = 0; j<n; j++) {
            sumVert += (*p)[i][j];
        }
    }
    //horizontal
    for (int i = 0; i<n; i++) {
        for (int j = 0; j<n; j++) {
            sumHorizont += (*p)[j][i];
        }
    }
    //diagonal
    for (int j = 0; j<n; j++) {
        sumDiagonal += (*p)[j][j];
    }
    if (sumVert != sumHorizont || sumDiagonal*n != sumVert || sumDiagonal*n != sumHorizont) {
        return false;
    } 

    return true;
}


int main(){
    int n;
    scanf("%d",&n);
    if(n > N){
        printf("Error\n");
        return 1;
    }
    int pole1[20][20];
    int **pole2 = (int**)malloc(sizeof(int*)*n);
    for (int i = 0; i<n; i++) {
        pole2[i] = (int*)malloc(sizeof(int) * n);
        memset(pole2[i], 0, sizeof(int)*n);
    }
    
    for (int i = 0; i<n; i++) {
        for (int j = 0; j<n; j++) {
            scanf("%d",&pole2[i][j]);
        }
    }
    for (int i = 0; i<n; i++) {
        for (int j = 0; j<n; j++) {
            printf("%d ",pole2[i][j]);
        }    
        printf("\n");
    }

    if(magic(&pole2,n)){
        printf("Magic\n");
    }
    for(int i = 0;i<n;i++){
        free(pole2[i]);
        pole2[i] = NULL;
    }
    free(pole2);
    pole2 = NULL;
    return 0;
}
