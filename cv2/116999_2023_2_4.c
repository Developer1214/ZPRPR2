#include<stdio.h>

void vypis(void(*function)(int*,int*,int*),int *a,int *b,int *r){
    function(a,b,r);
    if(r == NULL){
        return;
    }
    printf("Vysledok je : %d\n",*r);
}

void nacitanie(int *a, int *b,int *r){
    scanf("%d %d",a,b);
}

void scitanie(int *a, int *b, int *r){
    *r = *a + *b;
}
void odcitanie(int *a, int *b, int *r){
    *r = *a - *b;
}
void nasobenie(int *a, int *b, int *r){
    *r = *a * *b;
}
void delenie(int *a, int *b, int *r){
    *r = *a / *b;
}

void zisti(char* ch,int *m,int *v,int *c,int *z){
    if(*ch >= 'a' && *ch <= 'z'){
        *m = *m + 1;
    }else if(*ch >= 'A' && *ch <= 'Z'){
        *v = *v + 1;
    }else if(*ch >= '0' && *ch <= '9'){
        *c = *c + 1;
    }else if(*ch == ' ' || *ch == '\n' || *ch == '-'){
        *z = *z + 1;
    }

}
void subor(FILE *f,char * ch,int *m,int *v,int *c,int *z){
    f = fopen("vstup.txt", "r");
    if(f == NULL){
        printf("Subor sa nepodarilo otvorit\n");
        return;
    }
    while(feof(f) == 0){
        *ch = fgetc(f);
        zisti(ch,m,v,c,z);
    }
    fclose(f);
    printf("mali - %d velke - %d cisla - %d znaky - %d\n",*m,*v,*c,*z);
}



int main(){
    int a,b,r;
    while(1){
        char input;
        int m=0,v=0,c=0,z=0;
        char ch;
        FILE *f;

        printf("c - nacitanie cisel\ns - scitanie cisiel\no - odcitanie cisiel\nn - nasobenie cisiel\nd - delenie cisiel\np - praca so suborom\ne - koniec\n");
        scanf(" %c", &input);
        switch(input){
            case 'c': vypis(nacitanie,&a,&b,NULL); break;
            case 's': vypis(scitanie,&a,&b,&r); break;
            case 'o': vypis(odcitanie,&a,&b,&r); break;
            case 'n': vypis(nasobenie,&a,&b,&r); break;
            case 'd': vypis(delenie,&a,&b,&r); break;
            case 'p': subor(f,&ch,&m,&v,&c,&z); break;
            case 'e': return 0;
            default: printf("Operacia nie je podporovana\n\n");
        }
    }
    return 0;
}
