#include<stdio.h>
#include<math.h>

void kvadraticka_rovnica(double a,double b, double c, int *cislo, double *x1, double *x2){
    double d = pow(b,2) - (4 * a * c);
    if(d<0){
        *cislo = 0;
    }
    else if(d==0){
        *cislo = 1;
        *x1 = (-b)/(2*a);
        return;
    }
    else{
        *cislo = 2;
        *x1 = (-b + sqrt(d))/(2*a);
        *x2 = (-b - sqrt(d))/(2*a);
        return;
    }
}

int main(){
    double a,b,c;
    double x1,x2;
    int cislo;
    scanf("%lf %lf %lf", &a, &b, &c);
    kvadraticka_rovnica(a,b,c,&cislo,&x1,&x2);
    if(cislo == 1){
        printf("Rovnica ma jedno riesenie: %0.2lf\n", x1);
    }
    else if(cislo == 2){
        printf("Rovnica ma dve riesenia: %0.2lf %0.2lf\n", x1, x2);
    }
    else{
        printf("Rovnica nema riesenie\n");
    }
    return 0;
}
