#include<stdio.h>
#include<stdlib.h>


void vymen_ukazovatele(int** a,int** b){
    int *tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}


int main(){
    int a,b;
    scanf("%d %d", &a, &b);
    int *p = &a;
    int *q = &b;
    printf("Do funkcie a-%p %d || b-%p %d\n",p,a,q,b);
    vymen_ukazovatele(&p,&q);
    printf("Po funkcie a-%p %d || b-%p %d\n",p,a,q,b);
    

    return 0;
}
