#include <stdio.h>
#include <stdlib.h>

char * bin2String(unsigned dec){
    char *binary = (char *)malloc(sizeof(char) * 64);
    int idx = 0;
    unsigned num = dec;
    while(num > 0){
        binary[idx] = (num % 2) + '0';
        num /= 2;
        idx++;
    }
    binary[idx] = '\0';

    for(int i = idx-1,j = 0;j<i;i--,j++){
        char tmp = binary[j];
        binary[j] = binary[i];
        binary[i] = tmp;
    }
    return binary;
} 

int main(){
    unsigned dec = 0;
    scanf("%u",&dec);          
    char * result = bin2String(dec);
    
    printf("Result: %s\n",result);    
    
    free(result);
    result = NULL;
    return 0;
}
