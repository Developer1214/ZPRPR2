#include <stdio.h>
#include <stdlib.h>

void dec2bin(unsigned num){
    unsigned bin[64];
    unsigned index = 0;

    for(int i = 0;;i++){
        bin[i] = num%2;
        num /= 2;
        if(num <= 0){
            break;
        }
        index = i;
        printf("Num - %d\n",num);
    }
    printf("bin: ");
    for(int i = index + 1;i>=0;i--){
        printf("%d",bin[i]);
    }
    printf("\n");

}

int main(){
    unsigned cislo;
    scanf("%d",&cislo);
    dec2bin(cislo);
    return 0;
}
