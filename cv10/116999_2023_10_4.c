#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int power(int base, int exponent) {
    int result = 1;
    for (int i = 0; i < exponent; i++) {
        result *= base;
    }
    return result;
}

int binstring2dec(char *str) {
    int dec = 0;
    int len = strlen(str);

    for (int i = 0; i < len; i++) {
        dec += (str[len - 1 - i] - '0') * power(2, i);
    }

    return dec;
}

int main() {
    char *str = malloc(sizeof(char) * 65);
    scanf("%s", str);
    str[64] = '\0';
    printf("Result: %d\n", binstring2dec(str));
    free(str);
    str = NULL;
    return 0;
}
