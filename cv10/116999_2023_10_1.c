#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define mocnina_3(x) ( (x)*(x)*(x) )

#define ITERATION 3

int main(){
    float cislo1 = 0;
    float cislo2 = 0;
    char znak;

    for(int i = 0;i<ITERATION;i++){
        char * buf = malloc(sizeof(char)*128);
        fgets(buf, 127, stdin);
        int size = sscanf(buf, "%c %f %f",&znak,&cislo1,&cislo2);
        switch (size) {
            case 2: 
                if(znak == 'c'){
                    printf("%d\n",(int)mocnina_3(cislo1));
                    break;
                }else if(znak == 'r'){
                    printf("%.3f\n",mocnina_3(cislo1)); 
                    break;
                }
            case 3:
                switch (znak) {
                    case '+':
                        printf("%d\n",(int)mocnina_3(cislo1+cislo2));
                        break;
                    case '*':
                        printf("%d\n",(int)mocnina_3(cislo1*cislo2));
                        break;
                    case '/':
                        if(cislo2 == 0){
                            printf("Nekonecne velke cislo\n");
                            break;
                        }
                        printf("%d\n",(int)mocnina_3(cislo1/cislo2));
                        break;
                    case '-':
                        printf("%d\n",(int)mocnina_3(cislo1-cislo2));
                        break;
                }
        };
        free(buf);
        buf = NULL;
    }
    return 0;
}
