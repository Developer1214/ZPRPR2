#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int position(unsigned int x) {
    int position = -1;
    while (x > 0) {
        position++;
        x >>= 1;
    }
    return position;
}

unsigned int invert(unsigned int x,int i,int n){
    unsigned int result = (1u << n) - 1; //0000011
    result<<= (position(x) - i  - n + 1); //0110000(3 << 4)
    result ^= x; //1111011
                 //0110000
                 //1001011
                 //75 v dec
    
    return result;
}


int main(){
    int i,n;
    unsigned int x;

    scanf("%d %d %d",&x,&i,&n);
    printf("Result - %d\n",invert(x,i,n));
    return 0;
}
