#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//
//
//
//Počet mien je možné zmeniť tu!!!!!!
//
#define PEOPLE 100

typedef struct{
    char * name;
    int counter;
}Visitor;


void checkNames(Visitor * people,char * name,int j){
    for(int i = 0;i<j;i++){
        if(strcmp(name, people[i].name) == 0){
            people[i].counter++;
        }
    }
}   

int main(){


    Visitor * people = malloc(PEOPLE*sizeof(Visitor));     
    for(int i = 0;i<PEOPLE;i++){
        people[i].counter = 0;
        people[i].name = (char*)malloc(sizeof(char)*50);
    }
    for(int i = 0;i<PEOPLE;i++){
        fscanf(stdin, "%49s",people[i].name);
        checkNames(people,people[i].name,i);
    }
    int hfCounter = 0; 
    char * hfName;
    int multiNameFreq = 0;
    for(int i = 0;i<PEOPLE;i++){
        if(people[i + 1].name == NULL){
            break;
        }
        if(people[i].counter > hfCounter){
            hfCounter = people[i].counter;
            hfName = people[i].name;
        }else if(people[i].counter == hfCounter){
            multiNameFreq++;
        }

    }
    if(multiNameFreq == 0)printf("Most used name is %s\n",hfName);
    else printf("There is %d names with freq %d\n",multiNameFreq+1,hfCounter+1);
    int c = 0;
    for(int i = 0;i<PEOPLE;i++){
        if(people[i].counter == multiNameFreq)
        printf("%d - %s\n",++c,people[i].name);
    }
    for(int i = 0;i<PEOPLE;i++){
        free(people->name);
        people->name = NULL;   
    }
    free(people);
    people = NULL;

    return 0;
}
