#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct{
    char*   name;
    char*   manuf;
    int     count;
    float   price;
} Item;

void addProduct(Item **products,int *count,Item * newProduct){
    (*count)++;
    *products = (Item*)realloc(*products, *count * sizeof(Item));   
    int idx = 0;
    while((idx < (*count) - 1) && strcmp(newProduct->name,(*products)[idx].name) > 0){
        idx++;
    }
    for(int i = (*count) - 1;i>idx;i--){
        (*products)[i] = (*products)[i-1];
    }
    (*products)[idx] = *newProduct;
}


int main() {
    int productCount = 0;
    Item *products = NULL;
    
    printf("Enter productName, manufacturerName, quantity, price.\nType 'q' as name to stop:\n");

    while (1) {
        Item newProduct;
        newProduct.name = (char*)malloc(sizeof(char)*50);   
        newProduct.manuf = (char*)malloc(sizeof(char)*50);
        newProduct.count = 0;
        newProduct.price = 0.00;

        if (scanf("%49s", newProduct.name) != 1) {
            printf("Invalid input. Exiting.\n");
            break;
        }
        if (strcmp(newProduct.name, "q") == 0) {
            break;
        }
        if (scanf("%49s %d %f", newProduct.manuf, &newProduct.count, &newProduct.price) != 3) {
            printf("Invalid input. Exiting.\n");
            break;
        }
        addProduct(&products, &productCount, &newProduct);
    }

    printf("\nProduct list:\n");
    for (int i = 0; i < productCount; i++) {
        printf("%s %s %d %.2f\n", products[i].name, products[i].manuf, products[i].count, products[i].price);
    }
    for (int i = 0; i < productCount; i++) {
        free(products[i].name);
        free(products[i].manuf);
    }
    free(products);
    return 0;
}
