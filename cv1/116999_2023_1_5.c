#include<stdio.h>


int main(){
    FILE *f = fopen("text.txt","r");
    if(f == NULL){
        printf("Nepodarilo sa otvorit subor");
        return 1;
    }
    int counter = 0;
    while(feof(f) == 0){
        char c = fgetc(f);
        if(c == '*'){printf("Koniec\n"); break;}
        if(c == 'x' || c == 'X') printf("Precital som X\n");
        if(c == 'y' || c == 'Y') printf("Precital som Y\n");
        if(c == '#' || c == '$' || c == '&') printf("Precital som riadiaci znak\n");
        if(c == ' ') counter++;
    }
    fclose(f);
    printf("Pocet precitanych medzier: %d\n",counter);
    return 0;
}
