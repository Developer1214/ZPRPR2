#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>

long reverzne_cislo(long x){
    long vysledok = 0;
    long counter = 0;
    for(int i = 1;x % i !=x;i*=10){
        counter = i;
    }
    for(int i = 1;x % i != x;i *= 10){
        vysledok += ((x / i) % 10) * counter;
        counter /= 10;
    }
    return vysledok;
}


int main(){

    long x = 0;
    scanf("%ld",&x);
    long y = 0;
    scanf("%ld",&y);
    printf("%ld\n",reverzne_cislo(x));
    printf("Cislo %ld %s\n",x,(x == reverzne_cislo(x) ? "je palindrom" : "nie je palindrom"));
    printf("%ld\n",reverzne_cislo(y));
    printf("Cislo %ld %s\n",y,(y == reverzne_cislo(y) ? "je palindrom" : "nie je palindrom"));
    return 0;
}
