#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>


void mocnina(double x,int n){
    if(n < 0){
        printf("Nespravny vstup\n");
        return;
    }
    double vysledok = 1;
    if(n == 0){
        printf("%0.2lf^0 = 1\n",x);     
    }else {
        for(int i = 1;i<=n;i++){
            vysledok*=x;
            printf("%0.2lf^%d = %0.2lf\n",x,i,vysledok);
        }
    }
}

int main(){
    double x;
    int n;
    scanf("%lf %d",&x,&n);
    mocnina(x,n);
    return 0;
}
