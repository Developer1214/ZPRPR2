#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>


void vypis(FILE *f,int count){
    for(int i = 1;i <= count;i++){
        printf("%d:  ",i);
        fprintf(f,"%d:  ",i);
        for(int j = 0;j <= count-i;j++){
            printf("%d  ",count - j);
            fprintf(f,"%d  ",count - j);
        }
        fprintf(f,"\n");
        printf("\n");
    }
}
int main(){
    int count = 0;
    scanf("%d", &count);
    FILE *f = fopen("cvicenie1p2.txt","w");
    count < 1 || count > 15 ? printf("Cislo nie je z daneho intervalu\n") : vypis(f,count);
    fclose(f);
    return 0;
}
