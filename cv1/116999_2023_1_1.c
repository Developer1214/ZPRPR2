#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>



int main(){
    int count;
    int temp = 0;
    bool err = false;
    scanf("%d", &count);
    for(int i = 0;i < count;i++){
        int num;
        scanf("%d", &num);
        if((num > (temp * 2) || num < (temp / 2)) && i != 0){
            err = true;
        }
        if((num < 0 || num > 10) && i == 0){
            err = true;
        }
        temp = num;

    }
    if(err){
        printf("Postupnost nie je spravna\n");
    }
    else{
        printf("Postupnost je spravna\n");
    }
    return 0;
}
