
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define Pocet_Zaznamov 10 

void x(char **IDprispevku_1,  int velkost_pola){
    int k,l;
    scanf("%d %d",&k,&l);
    if(k <= 0 || l <= 0){
        printf("K a L nemozu byt nulove\n");
        return;
    }
    if(k>l){
        printf("K ma byt menej ako l\n");
        return;
    }
    if (k>=9) {
        printf("K ma byt menej ako 9\n");
        return;
    }
    if(l>=10){
        printf("L ma byt menej ako 10\n");
        return;
    }
    int * znaky = malloc(sizeof(int)*('Z'-'A'));
    int * num = malloc(sizeof(int)*('9'-'0'));
    for(int i = 0;i<velkost_pola;i++){
        for(int j = k-1;j<=l;j++){
            char ch = IDprispevku_1[i][j];
            if(ch >= 'A' && ch <= 'Z'){
                znaky[ch - 'A']++;
            } else if(ch >= '0' && ch <= '9'){
                num[ch - '0']++;
            }
        }
    }
    for(int i = 0;i<'Z'-'A';i++){
        if(znaky[i] != 0){
            printf("%c ",i + 'A');
        }    
    }
    for(int i = 0;i<'9'-'0';i++){
        if(num[i] != 0) {    
            printf("%c ",i + '0');
        }
    }
    printf("\n");
    for (int i = 0; i< 'Z'-'A'; i++) {
        if(znaky[i] != 0){    
            printf("%d ",znaky[i]);
        }
    }
    for (int i = 0; i< '9'-'0'; i++) {
        if(num[i] != 0){
            printf("%d ",num[i]);
        }
    }
    printf("\n");
    free(num);
    free(znaky);
    //printf("Cislo %c %d\n",'0','0');printf("Cislo %c %d\n",'1','1');printf("Cislo %c %d\n",'2','2');printf("Cislo %c %d\n",'3','3');
}

char** e (char ** MenaAutorov, int velkost_pola, char ZNAK, int *VelkostMENA){
    for(int i = 0;i<velkost_pola;i++){
        char * line = MenaAutorov[i];
        for(int j = 0;line[j];j++){
            if(line[j] == ZNAK){
                (*VelkostMENA)++;
            }
        }
    }

    char ** menaX = malloc(sizeof(char)*(*VelkostMENA));
    for(int i = 0;i<(*VelkostMENA);i++){
        char * tok = strtok(menaX[i], "#");
        menaX[i] = (char*)malloc(sizeof(char)*strlen(tok));
    }
    return menaX;
}


int main() {
	char **IDprispevku = NULL;
	char **MenaAutorov = NULL;
	int i = 0, j=0;
	char riad;

	IDprispevku = (char**)malloc(Pocet_Zaznamov * sizeof(char*));  
	for (i = 0; i < Pocet_Zaznamov; i++) {
		IDprispevku[i] = (char*)malloc(11 * sizeof(char));  
		switch(i){
		   case 0:  strcpy(*(IDprispevku + i),  "UP12345678"); break;
		   case 1:  strcpy(*(IDprispevku + i),  "UD12345678"); break;
		   case 2:  strcpy(*(IDprispevku + i),  "PD12345678"); break;
		   case 3:  strcpy(*(IDprispevku + i),  "PP12345678"); break;
	       case 4:  strcpy(*(IDprispevku + i),  "UP56556576"); break;
		   case 5:  strcpy(*(IDprispevku + i),  "UP12345678"); break;
		   case 6:  strcpy(*(IDprispevku + i),  "PP12345678"); break;
		   case 7:  strcpy(*(IDprispevku + i),  "UP98765432"); break;
		   case 8:  strcpy(*(IDprispevku + i),  "UD12345678"); break;
		   case 9:  strcpy(*(IDprispevku + i),  "UP76523567"); break;
		   default:  strcpy(*(IDprispevku + i), "PP29384729"); 
		
		}
	}

	MenaAutorov = (char**)malloc(Pocet_Zaznamov * sizeof(char*));  
	for (i = 0; i < Pocet_Zaznamov; i++) {
     	switch(i){
		  case 0:{
		      MenaAutorov[i] = (char*)malloc((strlen("Julius Novak#A#Marek Kovac#K#") +1)  * sizeof(char)); 
			  strcpy(*(MenaAutorov + i), "Julius Novak#A#Marek Kovac#K#");
		    } break;
	     case 1:{
		     MenaAutorov[i] = (char*)malloc((strlen("Lucia Ondrejkova#K#Lukas Hvizdal#A#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Lucia Ondrejkova#K#Lukas Hvizdal#A#");
		   } break;
         case 2:{
		     MenaAutorov[i] = (char*)malloc((strlen("MARCEL VON TAKAC#K#ANDREA NOVOTNA#A#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "MARCEL VON TAKAC#K#ANDREA NOVOTNA#A#");
		   } break;
         case 3:{
		     MenaAutorov[i] = (char*)malloc((strlen("Ondrej Zatko#A#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Ondrej Zatko#A#");
		   } break;
        case 4:{
		     MenaAutorov[i] = (char*)malloc((strlen("Lucia Ondrejkova#A#Lukas Hvizdal#K#Janko Mrkvicka#S#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Lucia Ondrejkova#A#Lukas Hvizdal#K#Janko Mrkvicka#S#");
		   } break;
        case 5:{
		     MenaAutorov[i] = (char*)malloc((strlen("Janko Mrkvicka#A#Lena Von Der Brecht#S#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Janko Mrkvicka#A#Lena Von Der Brecht#S#");
		   } break;
       case 6:{
		     MenaAutorov[i] = (char*)malloc((strlen("Janko Mrkvicka#A#Hana Von Der Brecht#A#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Janko Mrkvicka#A#Hana Von Der Brecht#A#");
		   } break;
       default:  {
		     MenaAutorov[i] = (char*)malloc((strlen("Neviem Neviem#A#") +1)  * sizeof(char)); 
			 strcpy(*(MenaAutorov + i), "Neviem Neviem#A#");
		   } 		
		}
	}

    
	   
	while ( scanf(" %c", &riad) == 1 && riad != 'k'){
		switch(riad){
		   case 'v': {
	                   // vypis poli
						for (i = 0; i < Pocet_Zaznamov; i++) {
							printf("%s \n",IDprispevku[i]);
						}

						for (i = 0; i < Pocet_Zaznamov; i++) {
							printf("%s \n",MenaAutorov[i]);
						}				 
					 } break;
			 case 'a': { 
				        printf("a:");
					} break;
		     case 'x': { 
				         x(IDprispevku, Pocet_Zaznamov);
					} break;
             case 'e': { 
                        char znak;
                        int velkostmena = 0;
                        scanf("%1c",&znak);
				        e(MenaAutorov, Pocet_Zaznamov, znak, &velkostmena);
					} break;
		   
		}
	
	
	}
	   
	
	
	
	
	
	
 

	
    // uvolnenie poli
	for (i = 0; i < Pocet_Zaznamov; i++) {
	    free(*(IDprispevku + i));
		*(IDprispevku + i) = NULL;
		free(*(MenaAutorov + i));
		*(MenaAutorov + i) = NULL;
	}
	free(IDprispevku);
	IDprispevku = NULL;
	free(MenaAutorov);
	MenaAutorov = NULL;
	

	return 0;
}
